# readme.md - README for Ssepan.Io.Core

## About

Common library of i/o functions for C# .Net Core applications; requires ssepan.utility.core

### Purpose

To encapsulate common i/o functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

### History

6.1:
~update references

6.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.0:
~add readme

Steve Sepan
<sjsepan@yahoo.com>
9/9/2024
