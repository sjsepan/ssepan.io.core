﻿using System.IO;

namespace Ssepan.Io.Core
{
	public static class PathExtensions
    {
        /// <summary>
        /// When given a path, append trailing directory separator,
        ///  so that Path.GetFileName and Path.GetDirectoryName do not mistake last directory for a file.
        /// </summary>
        /// <param name="path">string</param>
        /// <returns>string</returns>
        public static string WithTrailingSeparator(this string path)
        {
            if (path.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                return path;
            }
            else
            {
                return Path.Combine(path, " ").Trim();
            }
        }
    }
}
