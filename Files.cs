﻿using System;
using System.IO;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Io.Core
{
    public static class Files
    {
        /// <summary>
        /// Read file into byte array.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool Read
        (
            string filePath, 
            ref byte[] bytes
        )
        {
            bool returnValue = default;
            FileStream fileStream = default;
            BinaryReader binaryReader = default;

            try
            {
                fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                binaryReader = new BinaryReader(fileStream);
                bytes = binaryReader.ReadBytes((int)fileStream.Length);

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                binaryReader?.Close();
                fileStream?.Close();
            }
            return returnValue;
        }

        /// <summary>
        /// Write byte array to file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool Write
        (
            string filePath,
			byte[] bytes
        )
        {
            bool returnValue = default;
            FileStream fileStream = default;
            BinaryWriter binaryWriter = default;

            try
            {
                fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
                binaryWriter = new BinaryWriter(fileStream);
                binaryWriter.Write(bytes);

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                binaryWriter?.Close();
                fileStream?.Close();
            }
            return returnValue;
        }

        /// <summary>
        /// write content to output file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="content"></param>
        /// <param name="errorMessage"></param>
        public static void WriteOutputFile
        (
            string filePath,
            string content,
            ref string errorMessage
        )
        {
            try
            {
				using StreamWriter streamWriter = new(filePath);
				streamWriter.Write(content);
			}
            catch (Exception ex)
            {
                errorMessage = ex.Message;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
    }
}
